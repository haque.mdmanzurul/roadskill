<?php get_header(); ?>
  
  <!-- Blog banner
  ======================================================================= -->
  <div class="blog-banner">
      <div class="container">
          <div class="row">
              <h1>BLOG</h1>
          </div>
      </div>
  </div>
  
  <!-- Blog title
  ===================================================================== -->
  <div class="blog-title">
      <div class="container">
          <div class="row">
              <div class="col-sm-8 col-md-offset-2">
                  <h2><b>Roadskills</b> BLOG</h2>
                  <h3>Click the articles on the left to find out read our latest BLOG articles</h3>
              </div>
          </div>
      </div>
  </div>
  
  <!-- blog item in row
  ================================================================= -->
  <div class="blog-itemA">
      <div class="container">
          <div class="row">
          
         	<?php get_template_part('loop'); ?>

             
             
          </div>
      </div>
  </div>
  
 
  
  
 <?php get_template_part('testimonial'); ?>
   
   
<?php get_footer(); ?>