<?php get_header(); ?>
  
  <!-- about banner
  ============================================================ -->
  <div class="about-banner">
      <div class="container">
          <div class="row">
              <div class="col-sm-2"><h1><?php the_title(); ?></h1></div><!-- end col -->
              <div class="col-sm-8 col-md-offset-1">
                  <img src="images/banner_image2.png" class="img-responsive" alt="">
              </div>
          </div>
      </div>
  </div>
  
  
  <!-- about road skills
  =================================================================== -->
  <div class="about-roadSkill">
      <div class="container">
          <div class="row">
          
         <?php get_template_part('loop','page'); ?>

             
          </div>
      </div>
  </div>
  
 
  
  
 
  
     
   
  <?php get_template_part('testimonial'); ?>
   
   
  <?php get_footer(); ?>