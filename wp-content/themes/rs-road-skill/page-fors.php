<?php
/*Template Name: Fors Page*/
get_header(); ?>
 
  <!-- FORS banner
  ======================================================================== -->
  <div class="fors-banner">
      <div class="container">
          <div class="row">
              <div class="col-sm-2"><h1><?php the_title( ); ?></h1></div><!-- end col -->
              <div class="col-sm-8"><img src="<?php echo get_field ('fors-img'); ?>" class="img-responsive" alt=""></div><!-- end col -->
          </div>
      </div>
  </div>
  
  <!-- FORS workshop title
  ================================================================= -->
  <div class="fors-workshop-title">
      <div class="container">
          <div class="row">
              <div class="col-sm-8 col-md-offset-2">
                  <h2><b>FORS:</b> Wrokshops</h2>
                  <h3>Road Skill are sponsoring the FORS TfL Workshops in 2015</h3>
              </div>
          </div>
      </div>
  </div>
  
  <!-- Developing road transport policy
  =================================================================== -->
  <div class="fors-transport-Policy">
      <div class="container">
          <div class="row">
              <div class="col-sm-8">
                  <?php if (have_posts()) : while (have_posts()) : the_post();?>
					<?php the_content( ); ?>
                  <?php endwhile; endif; ?>
                  
                   
              </div><!-- end col -->
              
        <!-- ------ side menual------ -->
              <div class="col-sm-4 fors-side-menual">
              
              
              <?php
				query_posts('cat=8&showposts=10');
				if (have_posts()) : while (have_posts()) : the_post(); ?>      
				  <div >  
                  <a href="#"><?php the_post_thumbnail( $size, $attr ); ?></a>
                   <?php the_excerpt();?>
                  <hr>
                </div> 
               <?php endwhile; else: ?>
				<?php _e('No Posts Sorry Enter Your Currect Category Id.'); ?>
				<?php endif; ?>
             
              </div><!-- end col -->
          </div>
      </div>
  </div>
   
  
   
   
  <?php get_template_part('testimonial'); ?>			 

<?php
get_footer();
?>



 