<?php

add_theme_support('menus');
// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary'   => __( 'Top primary menu', 'twentyfourteen' ),
		 
	) );

add_theme_support( 'post-thumbnails' );

add_image_size('post', 375, 150, true);

add_image_size('testimonial', 215, 215, true);

add_image_size('home-post', 258, 168, true);

add_image_size('services', 190, 190, true);

function custom_excerpt_length( $length ) {
	return 50;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type( 'testimonial',
		array(
			'labels' => array(
				'name' => __( 'Testimonial' ),
				'singular_name' => __( 'Testimonial' )
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'Testimonial'),
			'supports' => array('title', 'editor', 'thumbnail')
		)
	);
	
	register_post_type( 'home-post',
		array(
			'labels' => array(
				'name' => __( 'Home Post' ),
				'singular_name' => __( 'Home Post' )
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'home-post'),
			'supports' => array('title', 'editor', 'thumbnail')
		)
	);
	
	register_post_type( 'services',
		array(
			'labels' => array(
				'name' => __( 'Services' ),
				'singular_name' => __( 'Services' )
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'Services'),
			'supports' => array('title', 'editor', 'thumbnail')
		)
	);

	register_post_type( 'slider',
		array(
			'labels' => array(
				'name' => __( 'Slider Items' ),
				'singular_name' => __( 'Sliders' )
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'slider'),
			'supports' => array('title', 'editor', 'thumbnail')
		)
	);

	register_post_type( 'aboutblock',
		array(
			'labels' => array(
				'name' => __( 'About Blocks' ),
				'singular_name' => __( 'about-blocks' )
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'about-block'),
			'supports' => array('title', 'editor', 'thumbnail','custom-field')
		)
	);
}

register_sidebar( array(
'name' => __( 'Footer 1', 'zahantech' ),
'id' => 'footer1',
'before_widget' => '<div>',
'after_widget' => '</div>',
'before_title' => '<h2>',
'after_title' => '</h2>',
) );

register_sidebar( array(
'name' => __( 'Footer 2', 'zahantech' ),
'id' => 'footer2',
'before_widget' => '<div>',
'after_widget' => '</div>',
'before_title' => '<h2>',
'after_title' => '</h2>',
) );

register_sidebar( array(
'name' => __( 'Footer 3', 'zahantech' ),
'id' => 'footer3',
'before_widget' => '<div>',
'after_widget' => '</div>',
'before_title' => '<h2>',
'after_title' => '</h2>',
) );

register_sidebar( array(
'name' => __( 'Footer 4', 'zahantech' ),
'id' => 'footer4',
'before_widget' => '<div>',
'after_widget' => '</div>',
'before_title' => '<h2>',
'after_title' => '</h2>',
) );


register_sidebar( array(
'name' => __( 'Request a Call Back ', 'zahantech' ),
'id' => 'footercontact',
'before_widget' => '<div>',
'after_widget' => '</div>',
'before_title' => '<h3>',
'after_title' => '</h3>',
) );

register_sidebar( array(
'name' => __( 'Meet the Team ', 'zahantech' ),
'id' => 'meettheteam',
'before_widget' => '<div>',
'after_widget' => '</div>',
'before_title' => '<h2>',
'after_title' => '</h2>',
) );


register_sidebar( array(
'name' => __( 'Office Loaction Right Sideber ', 'zahantech' ),
'id' => 'contactrightsidelocation',
'before_widget' => '',
'after_widget' => '',
'before_title' => '<h3> ',
'after_title' =>  '</h3>',
) );
register_sidebar( array(
'name' => __( 'Follow Us Right Sideber ', 'zahantech' ),
'id' => 'contactrightfllowus',
'before_widget' => '',
'after_widget' => '',
'before_title' => '<h3> ',
'after_title' =>  '</h3>',
) );

?>