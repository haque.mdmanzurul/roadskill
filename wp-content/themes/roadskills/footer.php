
<!-- footer
================================================================== -->
<footer>
    <div class="container">
        <div class="row">
            <div class="footer-menu col-sm-2">
                <!--<ul>
                    <li><a href="#">HOME</a></li>
                    <li><a href="#">ABOUT</a></li>
                    <li><a href="#">BLOG</a></li>
                    <li><a href="#">CONTACT</a></li>
                    <li><a href="#">TERMS</a></li>
                    <li><a href="#">PRIVACY</a></li>
                    <li><a href="#">COOKIES</a></li>
                </ul>-->
                <?php dynamic_sidebar('footer1'); ?>
                <div class="footer-site-link">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/FORS_footer.png" alt="FORS">
                    <p>VISIT FORS WEBSITE</p>
                </div>
            </div>
            <div class="service-menual col-sm-2">
                <!--<ul>
                    <li><a href="#">FORS</a></li>
                    <li><a href="#">Workshops</a></li>
                    <li><a href="#">Accreditations</a></li>
                    <li><a href="#">Get FORS,Keep FORS</a></li>
                    <li><a href="#">FORS Gold</a></li>
                    <li><a href="#">FORS Silver</a></li>
                    <li><a href="#">FORS Bronze</a></li>
                </ul>-->
                <?php dynamic_sidebar('footer2'); ?>
                <div class="footer-site-link">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/edriving_footer.png" alt="EDRIVING">
                    <p>VISIT EDRIVING WEBSITE</p>
                </div>
            </div>
            <div class="col-sm-3">
                <!--<ul>
                    <li><a href="#">AUDITS</a></li>
                    <li><a href="#">Reduce Insurance Premiums</a></li>
                    <li><a href="#">Motor Risk Management</a></li>
                    <li><a href="#">"O" Licence Compliance</a></li>
                    <li><a href="#">Driving at Wrok</a></li>
                    <li><a href="#">Health and Safety in Road Haulage</a></li>
                    <li><a href="#">Fuel Economy Management</a></li>
                    <li><a href="#">Road Collision Investigation</a></li>
                    <li><a href="#">Simulated Post Fatal Collision Investigation</a></li>
                </ul>-->
                <?php dynamic_sidebar('footer3'); ?>
            </div>
            <div class="col-sm-2 driving-info">
                <!--<ul>
                    <li><a href="#">DRIVER TRAINING</a></li>
                    <li><a href="#">Driver CPC Training</a></li>
                    <li><a href="#">E-Driving</a></li>
                    <li><a href="#">Advanced Driving</a></li>
                </ul>-->
                <?php dynamic_sidebar('footer4'); ?>
            </div>
            <div class="footer-logo col-sm-1">
                <img src="<?php echo get_template_directory_uri(); ?>/images/RS-logo.png" class="img-responsive" alt="RS-Road skills">
            </div>
            <div class="col-sm-3 call-back">
            <?php dynamic_sidebar('footercontact'); ?>
                 <div class="clearfix"></div> 
                <ul>
                    <p>Connect with us via social media</p>
                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter_footer.png" alt="Twitter"></a></li>
                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook_footer.png" alt="Facebook"></a></li>
                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/linkedin_footer.png" alt="Linkeding"></a></li>
                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/youtube_footer.png" alt="youtube"></a></li>
                </ul>
            </div>
        </div><!-- end row -->
        <!-- new row -->
        <div class="row copy-write">
            <hr>
            <div class="col-sm-5"><p>All rights reserved &copy; <?php the_date('Y') ?> <?php bloginfo('name'); ?></p></div>
            <div class="col-sm-3 col-md-offset-4"><span>Site by<a href="http://loyaltymatters.co.uk">LoyaltyMATTERS</a></span></div>
        </div>
    </div>
</footer>


<!-- script
=============================================== -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php  echo get_stylesheet_directory_uri() ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php  echo get_stylesheet_directory_uri() ?>/slick/slick.min.js"></script>
<script type="text/javascript" src="<?php  echo get_stylesheet_directory_uri() ?>/js/javascript.js"></script>
</body>
</html>