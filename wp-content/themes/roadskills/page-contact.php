<?php
/*Template Name: Contact Page*/
get_header(); ?>
   
  <div class="contact-banner">
      <div class="container">
          <div class="row">
             <h1><?php the_title(); ?></h1>
          </div>
      </div>
  </div>
  
  <!-- contact information
  ======================================================================== -->
  <div class="contact-info">
      <div class="container">
          <div class="row">
    <!-- ------ email sending section-------------------- -->
              <div class="col-sm-8 mail-submition">
                  
                    <?php if (have_posts()) : while (have_posts()) : the_post();?>
					<?php the_content( ); ?>
                  <?php endwhile; endif; ?>
                    
                
                  
              </div><!-- ----end col------ -->
        <!-- -- office location and social book mark--- -->
              <div class="col-sm-4">
               
                 <div class="office-location">
                 <?php dynamic_sidebar('contactrightsidelocation'); ?>
                  
                 </div> 
                  
                  <div class="follow-us">
                      <?php dynamic_sidebar('contactrightfllowus'); ?>
                  </div>
                  
              </div><!-- ----end col------ -->
          </div>
      </div>
  </div>
  
 

  
   
   
  <?php get_template_part('testimonial'); ?>			 

<?php
get_footer();
?>



 