<?php 
/*Template Name: Home Page*/
get_header(); ?>
  
  
<!-- main slider
==================================================================== -->
<?php
wp_reset_query();
$args = array( 'post_type' => 'slider', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
$slides = "";
$indicators = "";
$i = 0;
while ( $loop->have_posts() ) : $loop->the_post();
    $image = get_field('slide_image');
    $title = get_the_title();
    $content = get_the_content();
    $indicators .= '<li data-target="#carousel-example-generic" data-slide-to="'.$i.'" class="active"></li>';

    if($i == 0)
        $active = 'active';
    else
        $active = '';
    $slides .= '<div class="item '.$active.'" style="background: url() no-repeat top left;">
               <img src="'.$image.'"/>
              <div class="carousel-caption">
                  <h1>'.$title.'</h1>
                  '.$content.'
              </div>
            </div>';
    $i++;
endwhile;
wp_reset_query();
?>

    <div id="carousel-example-generic" class="carousel slide " data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php
            echo $indicators;
            ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

            <?php
            echo $slides;
            ?>

        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    
   <!-- Read more about us
   ============================================================= -->
   <div class="more-about-roadSkill">
       <div class="container">
           <div class="row">
               <div class="col-sm-12">
                    <?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>

                            <?php the_content(); ?>


                    <?php endwhile; endif; ?>
               </div><!-- end col -->

           </div>
       </div>
   </div>
   
   <!-- road skills services
   ============================================================= -->
   <div class="service-details">
       <div class="container">
           <div class="row">
           
           <?php
				global $post;
				$args = array( 'posts_per_page' => -1, 'post_type'=> 'home-post', 'oderby'=>'id','order'=>'asc' );
				$myposts = get_posts( $args );
				foreach( $myposts as $post ) : setup_postdata($post); ?>
					
                    <div class="col-sm-3">
                   <?php the_post_thumbnail( 'home-post', array( 'class' => 'img-responsive' ) ); ?>
                   <h2><?php the_title(); ?></h2>
                   <p><?php the_excerpt(); ?></p>
                   <a href="<?php the_permalink() ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/read_more.png" class="img-responsive" alt="Read more >"></a>
               </div><!-- end col -->
                    
				<?php endforeach; ?>


                  
                  
                
             


               
              
           </div>
       </div>
   </div>
   <!-- serviec text and item
   =============================================================== -->
   
   <?php get_template_part('services'); ?>
   
  <?php get_template_part('testimonial'); ?>
   
   
   
  <?php get_footer(); ?>