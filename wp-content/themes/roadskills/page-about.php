<?php
/*Template Name: About Page*/
get_header(); ?>
 <!-- about banner
  ============================================================ -->
  <?php 
  $image = get_field('about_img');
  ?>
  <div class="about-banner" style="background: url(<?php echo $image['url']; ?>)">
      <div class="container">
          <div class="row">
              <div class="col-sm-2"><h1><?php the_title( ); ?></h1></div><!-- end col -->
              
          </div>
      </div>
  </div>
  
  
   <!-- about road skills
  =================================================================== -->
<div class="about-con"> 
 <?php $args = array( 'post_type' => 'aboutblock', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();    
    echo '<div class="about-roadSkill"> <div class="container"> <div class="row">';?>
    
   <div class="col-sm-6 text"> <h2><?php the_title( ); ?> </h2>
    <?php the_content( ); ?> </div>
    
    <div class="col-sm-6 videocon">
    	<iframe width="480" height="355" src="<?php echo get_field ('about_block_video'); ?>" frameborder="0" allowfullscreen></iframe>
    </div>
    
    
<?php    
   echo '</div></div></div>';
endwhile;
?>
</div>	

  
   <!-- meet the team
  ================================================================= -->
  <div class="meet-the-team">
      <div class="container">
         <div class="row">
             <div class="col-sm-8 col-md-offset-2">
                 <?php dynamic_sidebar('meettheteam'); ?>
                 
                
             </div>
         </div>
         
<!-- ------------- details about David somers and lan Brooks------------- -->
          <div class="row team-member">
          
          			 
          
            
             
             <?php
				query_posts('cat=7&showposts=10');
				if (have_posts()) : while (have_posts()) : the_post(); ?>      
				 
				 <div >  
				<div class="col-sm-2">
                  <?php the_post_thumbnail( $size, $attr ); ?> 
              	 </div><!-- end col -->
				<div class="col-sm-4">
                 <h3><?php the_title(); ?></h3>
                 <?php the_content();?>
              </div>
             </div> 
				
				<?php endwhile; else: ?>
				<?php _e('No Posts Sorry Enter Your Currect Category Id.'); ?>
				<?php endif; ?>
            
     <!---------------- just new demo div for slide ----------------------->
              
             
               
          </div><!-- tm -->
      </div>
  </div>
  
     
   
  
   
   
  <?php get_template_part('testimonial'); ?>			 

<?php
get_footer();
?>



 